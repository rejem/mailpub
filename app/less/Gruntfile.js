module.exports = function(grunt) {
    grunt.initConfig({
        less: {
            development: {
                options: {
                    paths: ["./*"],
                    compress: true
                },
                files: {
                    "../../www/css/main.css": "./main.less"
                }
            }
        },
        watch: {
            files: "./*",
            tasks: ["less"]
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('default', [ 'less', 'watch' ]);
};