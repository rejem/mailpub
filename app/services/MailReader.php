<?php
/* Richard Ejem (c) 2015 */
namespace MailPub;

class MailReader extends \Nette\Object {
    private $imapConnection = NULL;
    
    private $config;
    
    function __construct($config) {
        $this->config = $config;
    }

    function getHeaders() {
        return imap_headers($this->getConnection());
    }
    
    function getMessagesCount() {
        return imap_check($this->getConnection())->Nmsgs;
    }
    
    function getMessagesHeaders($start, $count) {
        $resultHeaders = [];
        $headers = $this->getRawHeaders($start, $count);
        foreach ($headers as $header) {
            $resultHeaders[] = (object)[
                'from'      =>  imap_utf8($header->from),
                'subject'   =>  imap_utf8($header->subject),
                'timestamp' =>  $header->udate,
                'id'        =>  $header->msgno,
                'uid'       =>  $header->uid,
            ];
        }
        return $resultHeaders;
    }
    
    function getRawHeaders($start, $count) {
        $actualStart = $this->getMessagesCount() - $start;
        if ($actualStart <= 0) {
            return [];
        }
        $actualEnd = max(1, $actualStart - $count + 1);
        return array_reverse(imap_fetch_overview($this->getConnection(), "$actualEnd:$actualStart"));
    }
    
    function getHtmlBody($message) {
        return str_ireplace('<script', '', $message['body']);
    }
    
    private function getConnection() {
        if ($this->imapConnection === NULL) {
            $this->imapConnection = imap_open(
                "{{$this->config['server']}:{$this->config['port']}/imap"
                    . ($this->config['ssl'] ? '/ssl' : '')
                    . '}INBOX',
                $this->config['user'],
                $this->config['password']
            );
        }
        return $this->imapConnection;
    }
    
    function parseMessage($messageNumber) {
        // thx to Anonymous @ http://php.net/manual/en/function.imap-fetchstructure.php
        $mbox = $this->getConnection();
        $struct = imap_fetchstructure($mbox, $messageNumber);

        $parts = $struct->parts;
        $i = 0;
        if (!$parts) { /* Simple message, only 1 piece */
            $attachment = array(); /* No attachments */
            $content = imap_body($mbox, $messageNumber);
        } else { /* Complicated message, multiple parts */

            $endwhile = false;

            $stack = array(); /* Stack while parsing message */
            $content = "";    /* Content of message */
            $attachment = array(); /* Attachments */

            while (!$endwhile) {
                if (empty($parts[$i])) {
                    if (count($stack) > 0) {
                        $parts = $stack[count($stack)-1]["p"];
                        $i     = $stack[count($stack)-1]["i"] + 1;
                        array_pop($stack);
                    } else {
                        $endwhile = true;
                    }
                }

                if (!$endwhile) {
                    /* Create message part first (example '1.2.3') */
                    $partstring = "";
                    foreach ($stack as $s) {
                        $partstring .= ($s["i"]+1) . ".";
                    }
                    $partstring .= ($i+1);

                    if (isset($parts[$i]->disposition) && (strtoupper($parts[$i]->disposition) == "ATTACHMENT" || strtoupper($parts[$i]->disposition) == "INLINE")) { /* Attachment or inline images */
                        $fileData = imap_fetchbody($mbox, $messageNumber, $partstring);
                        if ( $fileData != '' ) {
                            // handles base64 encoding or plain text
                            $decodedData = base64_decode($fileData);
                            if ( $decodedData === false ) {
                                $decodedData = $fileData;
                            }
                            $attachment[] = array("filename" => imap_utf8($parts[$i]->parameters[0]->value),
                                    "filedata" => $decodedData);
                        }
                    } elseif ((strtoupper($parts[$i]->subtype) == "PLAIN" && strtoupper($parts[$i+1]->subtype) != "HTML") /* plain text message */
                            || strtoupper($parts[$i]->subtype) == "HTML"
                            ) { 
                        $append = $this->bodyToUTF8(
                                imap_fetchbody($mbox, $messageNumber, $partstring),
                                $parts[$i]);
                        if (strtoupper($parts[$i]->subtype) == "PLAIN") {
                            $append = '<pre>'.htmlspecialchars($append).'</pre>';
                        }
                        $content .= $append;
                    }
                }

                if (!empty($parts[$i]->parts)) {
                    if ( $parts[$i]->subtype != 'RELATED' ) {
                        // a glitch: embedded email message have one additional stack in the structure with subtype 'RELATED', but this stack is not present when using imap_fetchbody() to fetch parts.
                        $stack[] = array("p" => $parts, "i" => $i);
                    }
                    $parts = $parts[$i]->parts;
                    $i = 0;
                } else {
                    $i++;
                }
            } /* while */
        } /* complicated message */
        return [
            'body' => $content,
            'attachments' => $attachment,
        ];
    }
    private function bodyToUTF8($body, $mimePart) {
        foreach ($mimePart->parameters as $param) {
            if (strtolower($param->attribute) === 'charset') {
                $charset = $param->value;
                break;
            }
        }
        $body = quoted_printable_decode($body);
        if (isset($charset)) {
            $body = mb_convert_encoding($body, 'utf-8', $charset);
        }
        return $body;
    }
}