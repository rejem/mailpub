<?php
use Instante\Application\Responses\VirtualFileResponse;
class HomepagePresenter extends BasePresenter
{
    const ITEMS_PER_PAGE = 10;

    /** @var \MailPub\MailReader @inject */
    public $mailReader;

    /** @persistent */
    public $page = 0;
    
    /** @persistent */
    public $messageId = 0;
    
    private $message = NULL;

    protected function startup() {
        parent::startup();
        if ($this->messageId !== 0) {
            $this->message = $this->mailReader->parseMessage($this->messageId);
        }
    }

    
    function renderDefault() {
        $this->template->messageHeaders = $this->mailReader->getMessagesHeaders(
                $this->page * self::ITEMS_PER_PAGE, self::ITEMS_PER_PAGE);
        if ($this->message !== NULL) {
            $this->template->messageBody = $this->mailReader->getHtmlBody($this->message);
            $this->template->attachments = $this->message['attachments'];
        }
        $this->template->page = $this->page;
        $this->template->maxPage = floor($this->mailReader->getMessagesCount() / self::ITEMS_PER_PAGE);
        if ($this->template->page > $this->template->maxPage
            || $this->template->page < 0) {
                $this->redirect('this', ['page' => 0]);
        }
    }
    
    function actionGetAttachment($attNum) {
        if (!isset($this->message['attachments'][$attNum])) {
            $this->error();
        }
        $att = $this->message['attachments'][$attNum];
        $this->sendResponse(new VirtualFileResponse(
                $att['filename'],
                function() use ($att) {
                    echo $att['filedata'];
                }));
    }
}
