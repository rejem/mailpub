<?php

use Kdyby\Doctrine\EntityManager;

abstract class BasePresenter extends Nette\Application\UI\Presenter {

    use \Kdyby\Autowired\AutowireProperties;

    /**
     * @param string $module
     * @return boolean
     */
    function isModuleCurrent($module) {
        if (!$a = strrpos($this->name, ':')) { // not in module
            return false;
        }

        return ltrim($module, ':') === substr($this->name, 0, $a);
    }

    function flashInfo($message) {
        return $this->flashMessage($message, 'info');
    }

    function flashWarning($message) {
        return $this->flashMessage($message, 'warning');
    }

    function flashSuccess($message) {
        return $this->flashMessage($message, 'success');
    }

    function flashDanger($message) {
        return $this->flashMessage($message, 'danger');
    }

}
