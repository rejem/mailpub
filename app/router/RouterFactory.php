<?php

namespace InstanteApp;

use Nette\Application\Routers\RouteList,
    Nette\Application\Routers\Route,
    Nette\Application\Routers\SimpleRouter;

class RouterFactory
{

    /**
     * @return \Nette\Application\IRouter
     */
    public function createRouter($isConsoleMode = FALSE, $useSecureRoutes = TRUE, $isDebugMode = FALSE)
    {
        $router = new RouteList();

        if (!$isConsoleMode)
        {
            $secureFlag = $useSecureRoutes ? Route::SECURED : 0;

            $router[] = new Route('index.php', 'Homepage:default', Route::ONE_WAY);

            if($isDebugMode) {
                $router[] = new Route('[<? .*>/]log/<filename [a-z0-9-]+>.html', function($presenter, $filename) {
                    $path = realpath(__DIR__ . '/../../log/' . $filename . '.html');
                    if(file_exists($path)) {
                        return new \Nette\Application\Responses\TextResponse(file_get_contents($path));
                    }
                });
            }

            $router[] = new Route('[<action get-attachment>][/<id>]', array(
                'presenter' => 'Homepage',
                'action' => 'default',
                'id' => NULL,
            ), $secureFlag);


        }

        return $router;
    }

}
