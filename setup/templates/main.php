<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Instante project deployment/setup</title>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

        <!--[if lt IE 9]>
          <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container-fluid" style="margin:10px">
            <div class="row">
                <div class="col-lg-6" style="float:right">
                    <iframe name="xxxxx" style="width:100%;height:600px"></iframe>
                </div>
                <div class="col-lg-6">
                    <h1>Instante setup</h1>
                    <?php foreach ($application->getTasks() as $id=>$task): ?>
                    <p><a class="btn btn-default" target="xxxxx" href="<?php echo htmlspecialchars($task->getLink(), ENT_QUOTES) ?>"><?php echo htmlspecialchars($task->getName()) ?></a></p>
                    <?php endforeach ?>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>