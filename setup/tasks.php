<?php
namespace Instante\Setup;
return new TaskList([
    'InitialConfig' =>
        [
            'name'=>'Initialize application',
            'flow' => 'NettePurgeCache DoctrinePurgeProxies ComposerInstall DoctrineMigrate DoctrineGenerateProxies', ],
    'ModelUpdate' =>
        [
            'name'=>'Update database model',
            'flow' => 'NettePurgeCache DoctrinePurgeProxies DoctrineMigrate DoctrineGenerateProxies', ],
    'ComposerInstall' =>
        ['name'=>'Compooser: install dependencies'],
    'ComposerUpdate' =>
        ['name'=>'Compooser: update to newest versions'],
    'ComposerUpdateLock' =>
        [
            'name'=>'Compooser: install new dependencies from composer.json',
        ],
    'DoctrineMigrate' =>
        ['name'=>'Doctrine: migrate schema to current version (only, no purging cache)'],
    'DoctrineCreateMigration' =>
        ['name'=>'Doctrine: create new migration'],
    'DoctrineGenerateProxies' =>
        ['name'=>'Doctrine: generate proxies'],
    'DoctrinePurgeProxies' =>
        ['name'=>'Doctrine: purge proxies'],
    'NettePurgeCache' =>
        ['name'=>'Nette: purge cache'],
]);