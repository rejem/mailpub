<?php
namespace Instante\Setup;
use Instante\Setup\Application;
use Instante\Setup\StringOutput;
use Symfony\Component\Console\Input\ArgvInput;

function consoleCommand(Application $context, $command) {
    if (!defined('INSTANTE_SIMULATED_CLI')) {
        define('INSTANTE_SIMULATED_CLI',TRUE);
    }
    $container = require $context->getAppRoot().'/setup/load-project.php';
    $argv = explode(' ', "x $command");
    $console = $container->getByType('Kdyby\Console\Application');
    $console->run(new ArgvInput($argv), $out = new StringOutput);
    return $out->getBuffer();
}

function purgeDirectory($dir) {
    if (!is_dir($dir)) return [];
    $result = [];
    $files = array_diff(scandir($dir), array('.', '..'));

    foreach ($files as $file) {
        if (is_dir("$dir/$file")) {
            $result = array_merge($result, removeDirectory("$dir/$file"));

        }
        else {
            unlink("$dir/$file");
            $result[] = "$dir/$file";
        }
    }
    return $result;
}

function removeDirectory($dir) {
    $result = purgeDirectory($dir);
    $result[] = $dir;
    rmdir($dir);
    return $result;
}
