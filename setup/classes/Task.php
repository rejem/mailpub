<?php
namespace Instante\Setup;
/**
 * @author Richard Ejem
 */
class Task {
    /** @var Command[] */
    private $commandList = NULL;

    /** @var array */
    private $taskDefinition;

    /** @var string */
    private $id;

    function __construct($id, $taskDefinition) {
        $this->id = $id;
        $this->taskDefinition = $taskDefinition;
    }

    public function getCommandList() {
        if ($this->commandList === NULL) {
            $this->initCommandList();
        }
        return $this->commandList;
    }

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->taskDefinition['name'];
    }

    public function getLink() {
        return '?do='.$this->getId();
    }


    private function initCommandList() {
        $this->commandList = [];
        $flow = isset($this->taskDefinition['flow']) ? explode(' ', $this->taskDefinition['flow']) : [$this->id];
        foreach ($flow as $command) {
            $command = '\\Instante\\Setup\\Commands\\'.$command;
            if (class_exists($command)) {
                $commandObject = new $command;
                if (!$commandObject instanceof Commands\ICommand) {
                    throw new \Exception("$command does not implement Instante\\Setup\\Commands\\ICommand");
                }
            }
            else {
                $commandObject = new Commands\FunctionCommand($command);
            }
            $this->commandList[] = $commandObject;
        }
    }
    # isset($tasks[$do]['flow']) ? explode(' ', $tasks[$do]['flow']) : [$do];
}
