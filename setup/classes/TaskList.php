<?php
namespace Instante\Setup;

/**
 * @author Richard Ejem
 */
class TaskList {
    private $tasks;
    public function __construct(array $tasks) {
        $this->tasks = [];
        foreach ($tasks as $id=>$task) {
            $this->tasks[$id] = new Task($id, $task);
        }
    }

    public function getTasks() {
        return $this->tasks;
    }


    public function process(Application $context) {
        $do = filter_input(INPUT_GET, 'do', FILTER_SANITIZE_STRING);
        $result = [];
        if ($do && isset($this->tasks[$do])) {
            $commands = $this->tasks[$do]->getCommandList();
            foreach ($commands as $command) {
                $result[] = $command->execute($context);
            }
        }

        return $result ? implode("\n\n", $result) : NULL;
    }
}
