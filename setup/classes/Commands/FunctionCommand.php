<?php
namespace Instante\Setup\Commands;
use Instante\Setup\Application;

require_once __DIR__.'/ICommand.php';

/**
 * @author Richard Ejem
 */
class FunctionCommand implements ICommand{
    private $functionName;
    public function __construct($functionName) {
        $this->functionName = $functionName;
    }
    public function execute(Application $context) {
        $fn = $this->functionName;
        return $fn($context);
    }
}
