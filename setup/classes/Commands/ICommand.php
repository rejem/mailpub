<?php
namespace Instante\Setup\Commands;

use Instante\Setup\Application;
/**
 *
 * @author Richard Ejem
 */
interface ICommand {
    public function execute(Application $context);
}
