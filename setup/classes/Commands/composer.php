<?php
namespace Instante\Setup\Commands;

use Composer\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\ArgvInput;
use Instante\Setup\StringOutput;

function createAndNormalizePath($dir) {
    if (!is_dir($dir)) {
        mkdir($dir, 0777, true);
    }
    return realpath($dir);
}

function getComposer() {
    putenv('COMPOSER_HOME='.  createAndNormalizePath(__DIR__.'/../../../temp/composer-home'));
    putenv('COMPOSER_CACHE_DIR='.  createAndNormalizePath(__DIR__.'/../../../temp/composer-cache'));
    $app = new Application();
    $app->setAutoExit(false);
    return $app;
}

/**
 * Actually install only new composer dependencies added to composer.json,
 * without updating other libraries
 *
 * @return string
 */
function ComposerUpdateLock() {
    getComposer()->run(new ArgvInput(explode(' ', 'composer update --lock')), $out = new StringOutput);
    return $out->getBuffer();
}

/**
 * Updates all composer dependencies to newest available versions
 *
 * @return string
 */
function ComposerUpdate() {
    getComposer()->run(new ArrayInput(['command'=>'update']), $out = new StringOutput);
    return $out->getBuffer();
}

/**
 * Installs dependencies as defined by composer.lock
 *
 * @return string
 */
function ComposerInstall() {
    getComposer()->run(new ArrayInput(['command'=>'install']), $out = new StringOutput);
    return $out->getBuffer();
}
