<?php
namespace Instante\Setup\Commands;
use Instante\Setup\Application;

function NettePurgeCache(Application $context) {
    $other = '';
    if (file_exists($f=$context->getAppRoot().'/temp/btfj.dat')) {
        $other .= "\n$f";
        unlink($f);
    }
    return "Removed:\n".implode("\n", \Instante\Setup\purgeDirectory($context->getAppRoot().'/temp/cache/')).$other;
}