<?php
namespace Instante\Setup\Commands;

use Instante\Setup\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Instante\Setup\StringOutput;

/**
 * Migrates database to current doctrine version
 *
 * @return string
 */
function DoctrineMigrate(Application $context) {
    return \Instante\Setup\consoleCommand($context, 'migrations:migrate --no-interaction');
}

/**
 * Creates new doctrine migration
 *
 * @return string
 */
function DoctrineCreateMigration(Application $context) {
    return \Instante\Setup\consoleCommand($context, 'migrations:diff');
}

/**
 * Generates doctrine proxies
 *
 * @return string
 */
function DoctrineGenerateProxies(Application $context) {
    return \Instante\Setup\consoleCommand($context, 'orm:generate-proxies');
}

/**
 * Removes generated doctrine proxies
 *
 * @return string
 */
function DoctrinePurgeProxies(Application $context) {
    return "Removed:\n".implode("\n", \Instante\Setup\purgeDirectory($context->getAppRoot().'/temp/proxies/'));
}