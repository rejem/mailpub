<?php

namespace Instante\Setup;

class Application {

    /** @var string */
    private $output;

    /** @var Task[] */
    private $tasks;

    private $appRoot;

    public function __construct($appRoot) {
        $this->appRoot = $appRoot;
    }

    public function run() {
        $this->checkAuthorization();
        $this->loadClasses();
        $this->tasks = [];
        $this->output = [];

        set_time_limit(1800);

        $taskList = require __DIR__ . '/tasks.php';
        $this->tasks = $taskList->getTasks();
        $this->output = $taskList->process($this);
        $this->handleOutput();
        $application = $this;
        require __DIR__.'/templates/main.php';
    }

    public function getOutput() {
        return $this->output;
    }

    public function getTasks() {
        return $this->tasks;
    }

    public function getAppRoot() {
        return $this->appRoot;
    }


    private function checkAuthorization() {
        if (!file_exists(__DIR__ . '/allowed_ips.txt')) {
            die('The application environment is not initialized yet. Run "php setup.php" from shell or manually create environment file, local.neon file and allowed_ips.txt file');
        }
        $allowed_ips = array_map(function($arg){return preg_replace('~^\s*|\s*$~', '', $arg); }, file(__DIR__.'/allowed_ips.txt'));
        if (!in_array(filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_SANITIZE_STRING), $allowed_ips)) {
            header('HTTP/1.0 403 forbidden');
            die('access denied');
        }
    }

    private function handleOutput() {
        if ($this->output !== NULL) {
            echo "<pre>{$this->output}</pre>";
            die();
        }
    }

    private function loadClasses() {
        require_once('phar://'.__DIR__.'/composer.phar/vendor/autoload.php');
        $this->doDirRecursive(__DIR__ . '/classes', function($classFile) {
            require_once $classFile;
        });
    }

    private function doDirRecursive($dir, callable $callback) {
        $d = dir($dir);
        while ($item = $d->read()) {
            if ($item == '.' || $item == '..') {
                continue;
            }
            if (is_dir("$dir/$item")) {
                $this->doDirRecursive("$dir/$item", $callback);
            } else {
                $callback("$dir/$item");
            }
        }
    }
}
