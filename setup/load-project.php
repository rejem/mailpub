<?php
namespace Instante\Setup;
/**
 * This file loads the project's bootstrap so that you have access to project's
 * Nette container (returned by requiring this file) and project's class loader.
 *
 * @author Richard Ejem
 */
if (!class_exists('Instante\Setup\____NetteContainerHolder')) {
    class ____NetteContainerHolder {
        public static $container = NULL;
    }
}
if (____NetteContainerHolder::$container === NULL) {
    ____NetteContainerHolder::$container = require_once __DIR__.'/../app/bootstrap.php';
}
return ____NetteContainerHolder::$container;