```
#!


                                                         
███╗   ███╗ █████╗ ██╗██╗     ██████╗ ██╗   ██╗██████╗   ~~~~~~~~
████╗ ████║██╔══██╗██║██║     ██╔══██╗██║   ██║██╔══██╗ '| T  T |'
██╔████╔██║███████║██║██║     ██████╔╝██║   ██║██████╔╝ '| |  | |-|
██║╚██╔╝██║██╔══██║██║██║     ██╔═══╝ ██║   ██║██╔══██╗  | |  | |_|
██║ ╚═╝ ██║██║  ██║██║███████╗██║     ╚██████╔╝██████╔╝  | L  J |
╚═╝     ╚═╝╚═╝  ╚═╝╚═╝╚══════╝╚═╝      ╚═════╝ ╚═════╝   L______J

```

1. Deploy application:
    a) install dependencies by typing `composer install` to command line in project root
    b) create file `app/config/environment` with single line containing one of(development|stage|production)
    c) Copy `app/config/local.neon.example` to `app/config/local.neon` and adjust needed configuration)
    d) Ensure that these folders exist and the www server has write access to them:
        - temp
        - log
        - temp/sessions


2.Develop/compile LESS:
as all PHP LESS compilers have been found too slow or not up to date,
we recommend using grunt for on-the-fly LESS development.

install node.js, then use shell commands:
```
# setup
your-project/app/less$ npm install -g grunt-cli
your-project/app/less$ npm install
# start watchdog
your-project/app/less$ grunt
```
the watchdog starts to automatically compile less on any change.