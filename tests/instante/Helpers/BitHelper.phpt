<?php
require __DIR__.'/../../init-test-nenv.php';
use Tester\Assert;
use Instante\Helpers\BitHelper;

$testString = chr(0b00000110).chr(0b11100001).chr(0b01100001).chr(0b11000000);

Assert::same(0b1, BitHelper::subBits($testString, 6, 1));
Assert::same(0b110, BitHelper::subBits($testString, 9, 3));

Assert::same(0b010110, BitHelper::subBits($testString, 14, 6));

Assert::same(0b101110000101, BitHelper::subBits($testString, 6, 12));