<?php
require __DIR__.'/../../init-test-nenv.php';
use Tester\Assert;
use Instante\Utils\AutoGetSet;

class Foo extends \Nette\Object{
    use AutoGetSet;
    
    /** @getter @setter */
    protected $value;

    /** @getter */
    protected $ro;

    /** @setter */
    protected $wo;

    /** @getter */
    protected $custom = 'bad';

    public function getCustom() {
        return 'good';
    }
}

class Bar {
    use AutoGetSet;
}

$c = new Foo;

$c->value = 'x';
Assert::same('x', $c->value);
Assert::same('good', $c->custom);
Assert::throws(function() use($c){
    $c->ro = 'x';
}, 'Nette\MemberAccessException');
Assert::throws(function() use($c){
    $x = $c->wo;
}, 'Nette\MemberAccessException');

Assert::throws(function(){
    $wrong = new Bar;
}, 'LogicException');