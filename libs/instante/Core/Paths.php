<?php
namespace Instante\Core;

/**
 * Paths container for Instante project
 *
 * @author Richard
 */
class Paths {
    protected $dirs;
    function __construct($dirs) {
        $this->dirs = $dirs;
    }
    function &__get($name) {
        if (isset($this->dirs[$name])) return ($this->dirs[$name]);
        if (isset($this->dirs[$name.'Dir'])) return ($this->dirs[$name.'Dir']);
        throw new \Nette\InvalidArgumentException("Directory constant '$name' not defined");
    }
}
