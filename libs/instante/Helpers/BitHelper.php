<?php

/* (c) Instante contributors 2014 */

namespace Instante\Helpers;

/**
 * Description of BitHelper
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
class BitHelper {
    use \Instante\Utils\StaticClass;

    /**
     * Extract sequence of bits from string
     *
     * @param string $str
     * @param int start bit
     * @param int number of bits; must be lower than PHP bit size of integer (32 or 64 bits)
     * @return int
     * @throws \Nette\InvalidArgumentException
     */
    static function subBits($str, $start, $length) {
        if ($length > 8*PHP_INT_SIZE) {
            throw new \Nette\InvalidArgumentException("bits length cannot be currently bigger than php int bit size");
        }
        $startByte = (int) ($start / 8);
        $startBit = $start % 8;
        $end = $start + $length - 1;
        $endByte = (int) ($end / 8);
        $endBit = $end % 8;
        if ($startByte === $endByte) {
            $number = ord($str[$startByte]) >> (7-$endBit) & ((2 << ($endBit-$startBit)) - 1);
        }
        else {
            $number = ord($str[$startByte]) & ((2 << (7-$startBit)) -1);
            for ($b = $startByte + 1; $b < $endByte; ++$b) {
                $number <<= 8;
                $number |= ord($str[$b]);
            }
            $number <<= $endBit+1;
            $number |= (ord($str[$endByte]) >> (7-$endBit)) & ((2 << $endBit) -1);
        }
        return $number;
    }
}
