<?php

/* (c) MistyLab 2014 */

namespace Instante\Helpers;

/**
 * Description of DateTime
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
class DateTimeHelper {
    use \Instante\Utils\StaticClass;
    static function parse($string) {
        if ($string == '')  return NULL;
        $string = str_replace('. ', '.', $string);
        return new \DateTime($string);
    }
}
