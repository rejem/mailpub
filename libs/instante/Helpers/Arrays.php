<?php

namespace Instante\Helpers;

/**
 * Supplementary functions for handling arrays
 *
 * @author Richard Ejem
 * @package Instante
 */
class Arrays {

    /**
     * Maps values from a list of objects into key-value pairs.
     *
     * @param array|\Iterator $values
     * @param string $valueId
     * @param string $keyId
     * @return array
     */
    public static function createKeyMap($values, $valueId = 'name', $keyId = 'id') {
        $pairs = array();
        foreach ($values as $value) {
            $pairs[$value->{$keyId}] = $value->{$valueId};
        }
        return $pairs;
    }

    public static function sortCmp($a, $b) {
        if ($a == $b) return 0;
        return ($a < $b) ? -1 : 1;
    }

    public static function fetchValues(\Nette\Object $object, $values) {
        foreach ($values as $key => $val) {
            if (!property_exists($object, $key) && !method_exists($object, 'set' . ucfirst($key))) {
                throw new \Nette\InvalidArgumentException("class " . get_class($object) . " does not have property $key");
            }
            $object->$key = $val;
        }
    }

    /**
     * Improved version of PHP array_map function.
     *
     *  - can traverse through array or \Traversable (basically anything iterable by foreach)
     *  - returns array() if second argument is null
     *
     * @param \Traversable|array $traversable
     * @param callable $callback
     */
    public static function traversableMap($traversable, callable $callback, $preserveKeys = TRUE) {
        if ($traversable === NULL) return NULL;
        if (!$traversable instanceof \Traversable && !is_array($traversable)) {
            throw new \Nette\InvalidArgumentException('Argument must be \Traversable or array');
        }

        $result = [];
        foreach ($traversable as $key=>$val) {
            $x = $callback($val, $key);
            $result[$preserveKeys ? $key : NULL] = $x;
        }
        return $result;
    }
}
