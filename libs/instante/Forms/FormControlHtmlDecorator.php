<?php

/* (c) Instante contributors 2014 */

namespace Instante\Forms;

use Nette\Forms\Controls\BaseControl;

/**
 *
 * @author Richard
 */
class FormControlHtmlDecorator {
    function __construct() { throw new \Nette\StaticClassException; }
    static function register() {
        BaseControl::extensionMethod('addDataFlag', function(BaseControl $that, $name) {
            $that->getControlPrototype()->addAttributes(['data-'.$name=>TRUE]);
            return $that;
        });
        BaseControl::extensionMethod('hasDataFlag', function(BaseControl $that, $name) {
            return isset($that->getControlPrototype()->attrs['data-'.$name]);
        });
        BaseControl::extensionMethod('removeDataFlag', function(BaseControl $that, $name) {
            unset($that->getControlPrototype()->attrs['data-'.$name]);
            return $that;
        });
    }
}
