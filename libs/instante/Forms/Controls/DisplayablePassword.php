<?php

/* (c) MistyLab 2014 */

namespace Instante\Forms\Controls;

use Nette\Forms\Controls\TextInput;
use Nette\Utils\Html;

/**
 * Description of DisplayablePassword
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
class DisplayablePassword extends TextInput {

    /** @var Html */
    private $fullControl;

    function __construct($label = NULL, $maxLength = NULL) {
        parent::__construct($label, $maxLength);
        $this->setType('password');
        $this->control->addClass('form-control');
        $this->fullControl = Html::el('span')
            ->addClass('input-group')
            ->add('<span class="input-group-btn"><span class="btn btn-default"><i class="glyphicon glyphicon-eye-open"></i></span></span>')
                ->addAttributes(['data-instante-displayable-password'=>TRUE]);
    }

    public function getInputPrototype() {
        return parent::getControlPrototype();
    }

    public function getControlPrototype() {
        return $this->fullControl;
    }

    public function getControl() {
        $control = clone $this->fullControl;
        $control->insert(0, parent::getControl());
        return $control;
    }
}
