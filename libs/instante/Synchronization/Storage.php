<?php
namespace Instante\Synchronization;

class Storage extends \Nette\Object {
    /** @var resource file handle */
    private $f;

    function __construct($f) {
        $this->f = $f;
    }

    public function read() {
        fseek($this->f, 0);
        $content = '';
        while (!feof($this->f)) {
            $content .= fread($this->f, 8192);
        }
        return $content;
    }

    public function write($content) {
        fseek($this->f, 0);
        ftruncate($this->f, 0);
        fwrite($this->f, $content);
    }
}