<?php
namespace Instante\Synchronization;

/**
 * Nette semaphore service.
 * 
 * Usage:
 * - install as a service to config.neon:
 *     - Instante\Synchronization\Semaphore(string tempDir)
 * - wrap critical sections to callback called through
 * $semaphore->semaphore(id, callback). This service will set a file semaphore
 * to ensure that at most one callback with the same semaphore id is ran
 * at once. A handle to a file that performs the synchronization is
 * passed as an argument to the callback which can be thread-safely read and
 * written.
 *
 * @package Secuplanner
 * @author Richard Ejem <richard(at)ejem.cz>
 */
class Semaphore extends \Nette\Object{
    private $dir;

    function __construct($dir) {
        $this->dir = $dir;
        if (!is_dir($dir)) {
            if (!mkdir($dir, 0777, true)) {
                throw new \Nette\IOException("Cannot create directory $dir needed for synchronization service");
            }
        }
    }

    public function synchronize($id, \Closure $closure) {
        $f = fopen("{$this->dir}/$id", 'c+');
        flock($f, LOCK_EX);
        $closure(new Storage($f));
        flock($f, LOCK_UN);
        fclose($f);

    }
}
