<?php

namespace Instante\Console\DI;

use Kdyby\Console\DI\ConsoleExtension AS KConsoleExtension;
use Nette\Configurator;
use Nette\DI\Compiler;

/**
 * Description of ConsoleExtension
 *
 * @author Richard Ejem
 */
class ConsoleExtension extends KConsoleExtension {

    public function __construct() {
        $this->defaults['disabled'] = PHP_SAPI !== 'cli' && !defined('INSTANTE_SIMULATED_CLI');
    }

    /**
     * @param \Nette\Configurator $configurator
     */
    public static function register(Configurator $configurator) {
        $configurator->onCompile[] = function ($config, Compiler $compiler) {
            $compiler->addExtension('console', new ConsoleExtension());
        };
    }

    public function loadConfiguration() {
        parent::loadConfiguration();
        $builder = $this->getContainerBuilder();
        $config = $this->getConfig($this->defaults);

        if ($config['disabled']) {
            return;
        }

        $builder->getDefinition($this->prefix('application'))
                ->setClass('Instante\Console\Application', array($config['name'], $config['version']));
    }

}
