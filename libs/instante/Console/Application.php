<?php

namespace Instante\Console;

use Kdyby\Console\Application as KApplication;
use Symfony\Component\Console\Application as SApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nette\DI\Container;

/**
 * @author Richard Ejem
 */
class Application extends KApplication {

    private $errorOutput = NULL;
    private $catchExceptions = TRUE;
    protected $serviceLocator;

    /**
     * @param string $name
     * @param string $version
     */
    public function __construct($name = Nette\Framework::NAME, $version = Nette\Framework::VERSION) {
        parent::__construct($name, $version);
        SApplication::setCatchExceptions(FALSE);
        $this->setCatchExceptions(TRUE);
    }

    public function injectServiceLocator(Container $sl) {
        $this->serviceLocator = $sl;
        parent::injectServiceLocator($sl);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     * @throws \Exception
     */
    public function run(InputInterface $input = NULL, OutputInterface $output = NULL) {
        $catchExceptions = $this->catchExceptions;
        $this->catchExceptions = FALSE;
        try {
            return SApplication::run($input, $output);
        } catch (\Exception $e) {
            if ($this->catchExceptions = $catchExceptions) {
                /** @var Nette\Application\Application $app */
                if ($app = $this->serviceLocator->getByType('Nette\Application\Application', FALSE)) {
                    $this->errorOutput = $output;
                    $app->onError($app, $e);
                } else {
                    $this->handleException($e, $output);
                }

                return max(min((int) $e->getCode(), 254), 254);
            } else {
                throw $e;
            }
        }
    }

    public function handleException(\Exception $e, OutputInterface $output = NULL) {
        $output = $output ? : ($this->errorOutput ? : NULL);
        parent::handleException($e, $output);
    }

    public function getCatchExceptions() {
        return $this->catchExceptions;
    }

    public function setCatchExceptions($catchExceptions) {
        $this->catchExceptions = (bool) $catchExceptions;
    }

}
