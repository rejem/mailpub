<?php

namespace Instante\Doctrine\Users;

use Nette\Security as NS;

/**
 * Users authenticator.
 *
 * @author     Richard Ejem
 */
class Authenticator extends \Nette\Object implements NS\IAuthenticator
{

    /**@var \Kdyby\Doctrine\EntityDao */
    private $userDao;

    function __construct(\Kdyby\Doctrine\EntityDao $userDao)
    {
        $this->userDao = $userDao;
    }

    /**
     * Performs an authentication
     * @param  array
     * @return User
     * @throws \Nette\Security\AuthenticationException
     */
    public function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;

        if ($username instanceof User)
        {
            return $username;
        }

        $user = $this->userDao->findOneBy(array('name'=>$username));

        if ($user === NULL)
        {
            throw new NS\AuthenticationException("User '$username' not found.", self::IDENTITY_NOT_FOUND);
        }
        if (!$user->isActive())
        {
            throw new NS\AuthenticationException("User '$username' is inactive.", self::NOT_APPROVED);
        }
        if (!$user->checkPassword($password))
        {
            throw new NS\AuthenticationException("Invalid password.", self::INVALID_CREDENTIAL);
        }

        return $user;
    }

}
