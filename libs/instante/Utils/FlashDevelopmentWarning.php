<?php

/* (c) Instante contributors 2014 */

namespace Instante\Utils;

/**
 * Finds a presenter from static Nette context and fires a flash warning.
 *
 * Designed to mark unimplemented or not fully implemented behavior.
 * You may consider this a disgusting break of any coding best practices,
 * but any usage of this method is meant to be only in development phase
 * of any project.
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
class FlashDevelopmentWarning {
    static function send($message) {
        if (!\Tracy\Debugger::isEnabled()) {
            \Tracy\Debugger::log(new \Nette\InvalidStateException(__CLASS__.' should not occur/be used in production mode.'));
            return;
        }
        $presenter = \Nette\Environment::getContext()->getByType('\Nette\Application\Application')
                ->getPresenter();
        if ($presenter->getReflection()->hasMethod('flashWarning')) {
            $presenter->flashWarning($message);
        }
        else {
            $presenter->flashMessage($message, 'warning');
        }
    }
}
