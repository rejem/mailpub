<?php

/* (c) Instante contributors 2014 */

namespace Instante\Utils;

/**
 * Trait for creating constructor placeholders for protected variables.
 *
 * Usage: place @getter and/or @setter annotation to a property docblock
 * to make a protected property readable/writable.
 * IMPORTANT: class using this trait must be descendant of \Nette\Object.
 * No check is performed because of performance and
 * a possible collision of trait constructors.
 */
trait AutoGetSet {
    public function &__get($name) {
        try {
            return parent::__get($name);
        }
        catch (\Nette\MemberAccessException $ex) {
            /* @var $r \Nette\Reflection\ClassType */
            $r = $this->getReflection();
            if (!$r->hasProperty($name)) throw $ex;
            if (!$r->getProperty($name)->hasAnnotation('getter')) throw $ex;

            $r->getProperty($name);
            return $this->$name;
        }
    }
    public function __set($name, $value) {
        try {
            parent::__set($name, $value);
        }
        catch (\Nette\MemberAccessException $ex) {
            /* @var $r \Nette\Reflection\ClassType */
            $r = $this->getReflection();
            if (!$r->hasProperty($name)) throw $ex;
            if (!$r->getProperty($name)->hasAnnotation('setter')) throw $ex;
            $this->$name = $value;
        }
    }
}