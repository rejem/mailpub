<?php

/* (c) Instante contributors 2014 */

namespace Instante\Utils;

/**
 * Nette Mailer decorator for fast and efficient mail sending.
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
class MailDecorator {
    use StaticClass;
    static function sendmail($recipients, $subject, $body, $sender = 'web@example.com') {
        $mail = new \Nette\Mail\Message;
        if ($sender !== NULL) {
            $mail->setFrom($sender);
        }
        $mail->setSubject($subject)
             ->setBody($body);
        if (!is_array($recipients)) {
            $recipients = [$recipients];
        }
        foreach ($recipients as $rcpt) {
            $mail->addTo($rcpt);
        }
        $mailer = new \Nette\Mail\SendmailMailer;
        $mailer->send($mail);
    }
}
