<?php

/* (c) Instante contributors 2014 */

namespace Instante\Utils;

use Nette\Utils\Callback;

/**
 * Description of BaseDecorator
 *
 * @author Richard
 */
class BaseDecorator {
    /** @var mixed the decorated object */
    protected $obj;

    function __construct($obj) {
        $this->obj = $obj;
    }

    public function __call($name, $arguments) {
        return call_user_func_array([$this->obj, $name], $arguments);
    }

    public static function __callStatic($name, $arguments) {
        return call_user_func_array([get_class($this->obj), $name], $arguments);
    }

    public function __get($name) {
        return $this->obj->$name;
    }

    public function __invoke() {
        return Callback::invokeArgs($this->obj, func_get_args());
    }

    public function __isset($name) {
        return isset($this->obj->$name);
    }

    public function __set($name, $value) {
        $this->obj->$name = $value;
        return $this;
    }

    public function __toString() {
        return (string)$this->obj;
    }

    public function __unset($name) {
        unset($this->obj->$name);
    }

}
