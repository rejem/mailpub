<?php

/* (c) Instante contributors 2014 */

namespace Instante\Utils;

/**
 * Trait for marking static class.
 *
 * @author Richard Ejem <richard@ejem.cz>
 */
trait StaticClass {
    function __construct() { throw new \Nette\StaticClassException; }
}
