<?php

namespace Instante;

/**
 * Nette boostrap environment configurator
 *
 * @author Richard Ejem
 */
class BootstrapEnvironment
{

    const ENV_DEVELOPMENT = 'development',
            ENV_STAGE = 'stage',
            ENV_PRODUCTION = 'production',
            IS_DEBUGGING_KEY = 'debugMode';

    /**
     * @param   string       path to config
     * @param   array        IP adresses to enable debugging
     * @return  stdClass     environment and isDebugMode values
     */
    public static function configure($configDir, $developersIps = array('127.0.0.1', '::1'))
    {
        $environment = self::detectEnvironment($configDir);
        $isDebugMode = self::detectDebugMode($developersIps, $environment);

        return (object) array(
                    'environment' => $environment,
                    'isDebugMode' => $isDebugMode,
        );
    }

    /**
     * @param	string config dir
     * @return	string|NULL
     */
    public static function detectEnvironment($configDir)
    {
        if (file_exists($f = $configDir . "/environment")) {
            return trim(file_get_contents($f));
        }
        else {
            die("The application is not configured to run in this environment - no environment file found.");
        }
    }

    /**
     * Detects if debug mode should be enabled.
     *
     * To enable debug mode from url query, use ?debugMode=1; to disable, use ?debugMode=0.
     * Configuration is then stored to cookie debugMode=yes|no
     *
     * @param   array   developer IPs: To allow debug mode, you MUST be on one of these IPs
     * @param   string  environment identifier: enumeration of self::ENV_*
     * @return  bool    true if debug mode was set
     */
    public static function detectDebugMode($developerIps, $environment = NULL)
    {
        $isDebugMode = FALSE;
        $isDebugModeAllowed = in_array(array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : php_uname('n'), $developerIps, TRUE);
        if ($isDebugModeAllowed)
        {
            //just a complicated but proof method to get cookie parameter
            $useFilter = (!in_array(ini_get('filter.default'), array('', 'unsafe_raw')) || ini_get('filter.default_flags'));
            $cookies = $useFilter ? filter_input_array(INPUT_COOKIE, FILTER_UNSAFE_RAW) : (empty($_COOKIE) ? array() : $_COOKIE);
            // (C) Nette Framework

            $isCookieSet = array_key_exists(self::IS_DEBUGGING_KEY, $cookies);
            $isQuerySet = array_key_exists(self::IS_DEBUGGING_KEY, $_GET);
            $isDebugMode = ($isCookieSet && $cookies[self::IS_DEBUGGING_KEY] == 'yes') || (!$isCookieSet && $environment == self::ENV_DEVELOPMENT);
            if ($isQuerySet) {
                $isDebugMode = (bool)$_GET[self::IS_DEBUGGING_KEY];
            }

            $cookieExpiration = new \DateTime('+1 day');
            setcookie(self::IS_DEBUGGING_KEY, $isDebugMode ? 'yes' : 'no', $cookieExpiration->format('U'), '/', NULL, FALSE, TRUE);
        }

        return $isDebugMode;
    }
}
