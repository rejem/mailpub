<?php
if (PHP_SAPI !== 'cli') {
    chdir(__DIR__.'/..');
    require __DIR__.'/../setup/Application.php';
    (new Instante\Setup\Application(__DIR__.'/..'))->run();
}
else {
    $stdin = fopen ("php://stdin","r");
    print "*** Instante setup script ***\n";
    print "Leave any field empty for no change\n";
    while(true) {
        print 'Write environment name ([d]evelopment|[s]tage|[p]roduction) > ';
        $line = strtolower(chop(fgets($stdin)));
        switch ($line) {
            case 'd':
                $line = 'development';
                break;
            case 's':
                $line = 'stage';
                break;
            case 'p':
                $line = 'production';
                break;
        }
        if (!in_array($line, array('development', 'stage', 'production', ''), true)) {
            print "invalid environment\n";
        }
        else {
            break;
        }
    }
    if ($line !== '') {
        file_put_contents(__DIR__.'/../app/config/environment', $line);
    }

    print 'enter developer IPs (only highly trusted) to access online setup, comma separated > ';
    $ips = str_replace(",", "\n", chop(fgets($stdin)));
    if ($ips !== '') {
        file_put_contents(__DIR__.'/../setup/allowed_ips.txt', $ips);
    }

    do {
        print 'update database info? type y or n, then <enter>';
        $answer = strtolower(chop(fgets($stdin)));
    } while (!in_array($answer, ['y', 'n']));
    if ($answer === 'y') {
        print 'enter database host (127.0.0.1 recommended instead of localhost) > ';
        $dbhost = chop(fgets($stdin));
        print 'enter database user > ';
        $dbuser = chop(fgets($stdin));
        print 'enter database password > ';
        $dbpass = chop(fgets($stdin));
        print 'enter database schema name > ';
        $dbschema = chop(fgets($stdin));
        
        $cfgfile = __DIR__.'/../app/config/local.neon';
        $cfg = file_exists($cfgfile) ? file_get_contents($cfgfile) : file_get_contents($cfgfile.'.example');

        if (preg_match('~^\\s*host:~mi', $cfg)) {
            $cfg = preg_replace('~^(\\s*host:).*$~mi', '$1 '.$dbhost, $cfg);
        }
        else {
            $cfg = preg_replace('~^(\\s*)dbname:~mi', "$1host: $dbhost\n$1dbname:", $cfg);
        }
        $cfg = preg_replace('~^(\\s*user:).*$~mi', '$1 '.$dbuser, $cfg);
        $cfg = preg_replace('~^(\\s*password:).*$~mi', '$1 '.$dbpass, $cfg);
        $cfg = preg_replace('~^(\\s*dbname:).*$~mi', '$1 '.$dbschema, $cfg);
        file_put_contents($cfgfile, $cfg);
    }


}