/* (c) Instante contributors 2014 */

$(function() {
    $(document).on('mousedown', '[data-instante-displayable-password] .btn', function() {
        $(this).closest('[data-instante-displayable-password]').find('input').attr('type', 'text');
    })
    .on('mouseup mouseleave', '[data-instante-displayable-password] .btn', function() {
        $(this).closest('[data-instante-displayable-password]').find('input').attr('type', 'password');
    });
});